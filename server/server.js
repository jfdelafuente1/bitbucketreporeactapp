
const express = require('express');
const PORT = 4000;
const path = require('path')
const app = express();
const publicPath = path.join(__dirname,"..", 'build');

//bodyparser setup
app.use(express.static(publicPath));

app.get('*', (req, res) => 
    res.send(`Node and express server running on port ${PORT}`)
)
app.listen(PORT, () =>
    console.log(`You server is running on port ${PORT}`))